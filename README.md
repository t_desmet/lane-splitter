README
======

USAGE
-----
Follow the following steps to build Lane Splitter:

    mkdir build
    cd build
    cmake ..
    make

To build the documentation, also run:

    make doc
    
To run the game, return to the root of the project and run it from there:

    cd ..
    ./bin/LaneSplitter

To view the documentation in the default web browser, run the following command:

    xdg-open doc/doxygen/html/index.html

