/*
 * game.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "game.h"
#include <cstdlib>
#include <chrono>
#include <ctime>
#include <thread>

namespace ls {

/**
 * \brief The constructor for the Game class.
 *
 * TODO: Make it so that this singleton can be constructed with parameters.
 */
Game::Game()
	: bRunning(false), bPause(false), fps(30), road(new model::Road(4, 12, 100, 10, 20, 1)), view(new view::SDLWindow(1000, 600, "Lane Splitter")), controller(
	        new controller::SDLController()), gameTime(0), level(GameLevel::kIntermediate)
{
	// set the current time as seed for the random number generator.
	std::srand(std::time(0));
}

/**
 * \brief The destructor for the Game singleton.
 */
Game::~Game()
{
	delete road;
	delete view;
	delete controller;
}

/**
 * \brief Get the instance of the Game singleton or create one if none exists yet.
 *
 * @return A reference to the instance of the Game singleton.
 */
Game& Game::getInstance()
{
	static Game instance; // Guaranteed to be destroyed.
	                      // Instantiated on first use.
	return instance;
}

/**
 * \brief Start the game loop.
 *
 * A view will be created and the model, view and controller will be updated
 * at the rate of the set frame-per-seconds.
 */
void Game::run()
{
	// put this singleton in running state
	bRunning = true;

	// create a window
	view->create();

	// Choose level
	//levelChooser();

	// store the time we start the first iteration
	std::chrono::time_point < std::chrono::system_clock > start = std::chrono::system_clock::now();

	// start computing frames while in running state
	while (bRunning) {
		// calculate time passed since last iteration
		float dt = std::chrono::duration_cast < std::chrono::milliseconds
		        > (std::chrono::system_clock::now() - start).count();
		// recalculate the start of this iteration
		start = std::chrono::system_clock::now();

		// update the whole game instance to the present
		update(dt);

		// compute the remaining time for this frame and wait before computing the next one
		std::chrono::duration<double, std::milli> delay(
		        1000. / fps - std::chrono::duration_cast < std::chrono::milliseconds
		                > (std::chrono::system_clock::now() - start).count());
		std::this_thread::sleep_for(delay);
	}

	view->close();
}

/**
 * \brief Stop the game loop.
 *
 * This is done by resetting this singleton's running state.
 */
void Game::stop()
{
	bRunning = false;
}

/**
 * \brief Pause the game.
 */
void Game::pause()
{
	bPause^=true;
}

/**
 * Getter for the player's health.
 *
 * @return The player's health.
 */
float Game::getPlayerHealth() const
{
	return road->getPlayerHealth();
}
/**
 * Setter for the player's health.
 *
 * @param health The health that should be set.
 */
void Game::setPlayerHealth(float health)
{
	 road->setPlayerHealth(health);
}

/**
 * Getter for the level of this game.
 *
 * @return The game level.
 */
GameLevel Game::getLevel() const
{
	return level;
}

/**
 * Setter for the level of this game.
 *
 * @param level The new game level.
 */
void Game::setLevel(GameLevel level)
{
	this->level = level;
}

/**
 * Getter for the time since the start of this game.
 *
 * @return The game time.
 */
float Game::getGameTime() const
{
	return gameTime;
}

/**
 * \brief Update model, view and controller for the given time interval.
 *
 * @param dt The time between the previous state and the one to compute now.
 */
void Game::update(float dt)
{
	updateController();
	if(!bPause) {
		updateModel(dt);
		updateView(dt);
		// increase game time
		gameTime += dt;
	}

	// stop the game loop if the player is dead
	if (!road->getPlayer().isAlive())
		stop();
}

/**
 * \brief Update the model for the given time interval.
 *
 * @param dt The time between the previous state and the one to compute now.
 */
void Game::updateModel(float dt)
{
	road->update(dt);
}

/**
 * \brief Update the view for the given time interval.
 *
 * @param dt The time between the previous state and the one to compute now.
 */
void Game::updateView(float dt)
{
	view->clear();
	view->drawRoad(*road);
	view->drawTime(gameTime);
	view->drawPlayerHealth(road->getPlayer().getHealth());
	view->drawPlayerAmmo(road->getPlayer().getAmmo());
	view->render();
}

/**
 * \brief Update the controller for the given time interval.
 *
 * @param dt The time between the previous state and the one to compute now.
 */
void Game::updateController()
{
	controller->handleInputEvents(*road);
}

}

