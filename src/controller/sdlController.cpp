/*
 * sdlController.cpp
 *
 *  Created on: 9-mei-2015
 *      Author: Tom De Smet
 */

#include "sdlController.h"
#include <SDL2/SDL.h>

namespace ls {
namespace controller {
std::list<InputEvent> SDLController::getEvents()
{
	// This list will hold all new events.
	std::list<InputEvent> events;

	// Allocate an SDL_Event before to loop.
	SDL_Event ev;
	// Poll for SDL events until there are none left.
	while (SDL_PollEvent (&ev)) {
		// Allocate a new input event
		InputEvent iEvent;

		// Add additional information depending on the event type
		switch (ev.type)
		{
		case SDL_QUIT:
			// Set event type to Closed.
			iEvent.type = InputEvent::EventType::kClosed;
			break;
		case SDL_KEYDOWN:
			// Set event type to KeyPressed.
			iEvent.type = InputEvent::EventType::kKeyPressed;
			// Set which key was pressed.
			switch(ev.key.keysym.sym)
			{
			case SDLK_LEFT:
				// Set key code to Left.
				iEvent.key.code = InputEvent::Key::Left;
				break;
			case SDLK_RIGHT:
				// Set key code to Right.
				iEvent.key.code = InputEvent::Key::Right;
				break;
			case SDLK_SPACE:
				// Set key code to Space.
				iEvent.key.code = InputEvent::Key::Space;
				break;
			case SDLK_p:
				/// Set key code to P.
				iEvent.key.code = InputEvent::Key::P;
			}
			break;
		default:
			// We didn't recognize the event, so set event type to unknown.
			iEvent.type = InputEvent::EventType::kUnknown;
			break;
		}
		// Add the new input event to the list.
		events.push_back(iEvent);
	}
	// Return all gathered events.
	return events;
}
}
}
