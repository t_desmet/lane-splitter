/*
 * sdlController.h
 *
 *  Created on: 9-mei-2015
 *      Author: Tom De Smet
 */

#ifndef CONTROLLER_SDLCONTROLLER_H_
#define CONTROLLER_SDLCONTROLLER_H_

#include "controller.h"

namespace ls
{
namespace controller {
/**
 * \brief This class specializes the Controller class for SDL 2.
 */
class SDLController : public Controller
{
public:
	std::list<InputEvent> getEvents();
};
}
}

#endif /* CONTROLLER_SDLCONTROLLER_H_ */
