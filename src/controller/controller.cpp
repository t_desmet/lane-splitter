/*
 * controller.cpp
 *
 *  Created on: 21-mei-2015
 *      Author: Tom De Smet
 */
#include "controller.h"
#include "../game.h"
#include "../model/road.h"

namespace ls
{
namespace controller
{
/**
 * \brief Handles all input events and updates the \ref ls::model::Road "model" and
 * \ref ls::Game "game" accordingly.
 *
 * @param road A reference to the model (road) to manipulate.
 */
void Controller::handleInputEvents(model::Road& road)
{
	// Get the current X coordinate of the player.
	float x = road.getPlayer().getXPos();

	// Loop through all input events.
	for (auto& event : getEvents()) {
		// Select the case for this event type.
		switch (event.type) {
		case controller::InputEvent::EventType::kClosed:
			// Close event, so stop the game.
			Game::getInstance().stop();
			break;
		case controller::InputEvent::EventType::kKeyPressed:
			// Key pressed event, now determine which key.
			switch (event.key.code) {
			case controller::InputEvent::Key::Left:
				// Move to the left if the player's not at the edge yet.
				if (x - 0.2 > -(int) road.getWidth() + 2 * road.getPlayer().getWidth())
					road.getPlayer().setXPos(x - 0.2);
				break;
			case controller::InputEvent::Key::Right:
				// Move to the right if the player's not at the edge yet.
				if (x + 0.2 < road.getWidth() - 2 * road.getPlayer().getWidth())
					road.getPlayer().setXPos(x + 0.2);
				break;
			case controller::InputEvent::Key::Space:
				// Shoot the gun
				road.playerFire();
				break;
			case controller::InputEvent::Key::P:
				// Pause the game.
				Game::getInstance().pause();
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}
}
}
