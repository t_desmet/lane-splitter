/*
 * controller.h
 *
 *  Created on: 2-mei-2015
 *      Author: Tom De Smet
 */

#ifndef CONTROLLER_CONTROLLER_H_
#define CONTROLLER_CONTROLLER_H_

#include "inputEvent.h"
#include <list>

namespace ls {
// Forward class declaration of model::Road.
namespace model {
class Road;
}

/**
 * \brief The namespace where all code for the Controller component resides.
 * This is the 'C' in 'MVC'.
 */
namespace controller {
/**
 * \brief The Controller class.
 *
 * It is responsible for detecting and handling all inputs from the user.
 * This class is abstract and has the virtual method getEvents that should
 * be specialized for a medialibrary such as SDL, SFML, DirectX, ...
 */
class Controller
{
public:
	virtual ~Controller() = default;
	void handleInputEvents(model::Road& road);

	/**
	 * \brief Returns a list of input events that the used multimedia library
	 * detected.
	 *
	 * Needs to be specialized for the used multimedia library.
	 *
	 * @return A list of gathered input events.
	 */
	virtual std::list<InputEvent> getEvents() = 0;
};
}
}

#endif /* CONTROLLER_CONTROLLER_H_ */
