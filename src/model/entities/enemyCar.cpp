/*
 * otherCar.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "enemyCar.h"
#include "player.h"

namespace ls {
namespace model {
void EnemyCar::collide(Entity& other)
{
	// Checks for collision with player.
	Player * player = dynamic_cast<Player *>(&other);
	if (player != NULL) {
		// Damage the player.
		player->setHealth(player->getHealth()-30);
		// Remove this enemyCar.
		this->kill();
	}
}

std::string EnemyCar::getName() const
{
	return "Other Car";
}

std::string EnemyCar::getSpritePath() const
{
	return "resources/sprites/otherCar.png";
}
}
}
