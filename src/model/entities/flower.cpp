/*
 * flower.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: tom
 */

#include "flower.h"
#include "enemyCar.h"

namespace ls {
namespace model {

std::string Flower::getName() const
{
	return "Bullet";
}

std::string Flower::getSpritePath() const
{
	return "resources/sprites/flower.png";
}

void Flower::collide(Entity& other)
{
	// Checks for collision with a car.
	Car * car = dynamic_cast<Car *>(&other);
	if (car != NULL) {
		// Damage car.
		car->setHealth(car->getHealth()-10);
		// Remove this flower.
		this->kill();
	}
}

}
}



