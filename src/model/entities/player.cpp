/*
 * player.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "player.h"

namespace ls {
namespace model {
/**
 * \brief The constructor for Player.
 *
 * @param xPos The X coordinate of the player.
 * @param yPos The Y coordinate of the player.
 * @param health The health of the player.
 * @param ammo The ammo of the player.
 * @param name The name of the player.
 */
Player::Player(unsigned int xPos, unsigned int yPos, float health, float ammo, std::string name)
	: Car(xPos, yPos, 0, health, ammo), name(name)
{
}

void Player::collide(Entity& other)
{
}

std::string Player::getName() const
{
	return "Player";
}

std::string Player::getSpritePath() const
{
	return "resources/sprites/playerCar.png";
}
}
}
