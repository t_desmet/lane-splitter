/*
 * player.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_PLAYER_H_
#define MODEL_ENTITIES_PLAYER_H_

#include "car.h"

namespace ls {
namespace model {
/**
 * \brief This class specializes Car to represent the player.
 */
class Player: public Car
{
public:
	Player(unsigned int x, unsigned int y = 100, float health = 100, float ammo = 10, std::string name="Player");
	void collide(Entity& other);
	std::string getName() const;
	std::string getSpritePath() const;
private:
	/**
	 * The name of the player.
	 */
	std::string name;
};
}
}

#endif /* MODEL_ENTITIES_PLAYER_H_ */
