/*
 * otherCar.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_ENEMYCAR_H_
#define MODEL_ENTITIES_ENEMYCAR_H_

#include "car.h"

namespace ls {
namespace model {
/**
 * \brief This class specializes Car to represent every autonomous enemy car.
 */
class EnemyCar: public Car
{
public:
	using Car::Car;
	void collide(Entity& other);
	virtual std::string getName() const;
	virtual std::string getSpritePath() const;
};
}
}

#endif /* MODEL_ENTITIES_ENEMYCAR_H_ */
