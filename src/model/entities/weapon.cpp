/*
 * weapon.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: tom
 */

#include "weapon.h"

namespace ls {
namespace model {

std::string Weapon::getName() const {
	return "Weapon";
}

void Weapon::update(float dt)
{
	yPos -= dt * velocity;
}

float Weapon::getWidth() const {
	return 0.25;
}

float Weapon::getHeight() const {
	return 0.25;
}

}
}


