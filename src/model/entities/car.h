/*
 * car.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_CAR_H_
#define MODEL_ENTITIES_CAR_H_

#include "entity.h"

namespace ls {
namespace model {
/**
 * \brief This is a specialization of Entity and represents every car in the game.
 */
class Car: public Entity
{
public:
	Car(float xPos, float yPos, float velocity, float health, float ammo);
	virtual ~Car();

	virtual std::string getName() const;
	virtual std::string getSpritePath() const = 0;
	void update(float dt);
	virtual void collide(Entity& other) = 0;
	float getWidth() const;
	float getHeight() const;
	float getAmmo() const;
	void setAmmo(float ammo);
	float getHealth() const;
	void setHealth(float health);

protected:
	/** The health of the car. */
	float health;
	/** Amount of ammo the car has. */
	float ammo;
};
}
}

#endif /* MODEL_ENTITIES_CAR_H_ */
