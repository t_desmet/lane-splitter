/*
 * bonus.h
 *
 *  Created on: Jun 17, 2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_BONUS_H_
#define MODEL_ENTITIES_BONUS_H_

#include "entity.h"

namespace ls{
namespace model {
/**
 * \brief This is a specialization of Entity and represents every bonus in the game.
 */
class Bonus: public Entity
{
public:
	using Entity::Entity;

	virtual void collide(Entity& other) = 0;
	void update(float dt);
	float getWidth() const;
	float getHeight() const;
	virtual std::string getName() const;
	virtual std::string getSpritePath() const = 0;
};

}
}

#endif /* SRC_MODEL_ENTITIES_BONUS_H_ */
