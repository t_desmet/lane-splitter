/*
 * entity.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_ENTITY_H_
#define MODEL_ENTITIES_ENTITY_H_

#include <string>

namespace ls {
namespace model {
/**
 * \brief This abstract class represents every entity in the game.
 */
class Entity
{
public:
	Entity(float xPos, float yPos, float velocity);
	virtual ~Entity();
	/**
	 * \brief Getter for this entity's name.
	 *
	 * @return This entity's name.
	 */
	virtual std::string getName() const = 0;
	/**
	 * \brief Getter for the path for this entity's sprite.
	 *
	 * @return The path for this entity's sprite.
	 */
	virtual std::string getSpritePath() const = 0;
	/**
	 * \brief Updates this entity for a given interval.
	 *
	 * @param dt The interval to update for.
	 */
	virtual void update(float dt) = 0;

	/**
	 * \brief Handle the collision with a given entity.
	 *
	 * @param other The entity with which this one is colliding.
	 */
	virtual void collide(Entity& other) = 0;

	/**
	 * \brief Getter for the width of this entity.
	 *
	 * @return The width of this entity.
	 */
	virtual float getWidth() const = 0;

	/**
	 * \brief Getter for the height of this entity.
	 *
	 * @return The height of this entity.
	 */
	virtual float getHeight() const = 0;

	bool collidesWith(const Entity& other) const;
	float getVelocity() const;
	void setVelocity(float velocity);
	float getXPos() const;
	void setXPos(float x);
	float getYPos() const;
	void setYPos(float y);
	bool isAlive() const;
	void kill();
protected:
	/**
	 * The horizontal position of this entity.
	 */
	float xPos;

	/**
	 * The vertical position of this entity.
	 */
	float yPos;

	/**
	 * The vertical velocity of this entity.
	 */
	float velocity;

	/**
	 * The alive state of this entity.
	 */
	bool alive;
};
}
}

#endif /* MODEL_ENTITIES_ENTITY_H_ */
