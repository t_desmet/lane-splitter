/*
 * weapon.h
 *
 *  Created on: Jun 18, 2015
 *      Author: tom
 */

#ifndef MODEL_ENTITIES_WEAPON_H_
#define MODEL_ENTITIES_WEAPON_H_

#include "entity.h"

namespace ls{
namespace model {
/**
 * \brief This is a specialization of Entity and represents every weapon in the game.
 */
class Weapon: public Entity
{
public:
	using Entity::Entity;

	virtual std::string getName() const;
	virtual std::string getSpritePath() const = 0;
	virtual void collide(Entity& other) = 0;
	void update(float dt);
	float getWidth() const;
	float getHeight() const;

};
}
}



#endif /* MODEL_ENTITIES_WEAPON_H_ */
