/*
 * flower.h
 *
 *  Created on: Jun 18, 2015
 *      Author: tom
 */

#ifndef MODEL_ENTITIES_FLOWER_H_
#define MODEL_ENTITIES_FLOWER_H_

#include "weapon.h"

namespace ls{
namespace model {
/**
 * \brief This class specializes Weapon to represent every flower shot.
 */
class Flower: public Weapon
{
public:
	using Weapon::Weapon;

	std::string getName() const;
	std::string getSpritePath() const;
	void collide(Entity& other);
};
}
}



#endif /* MODEL_ENTITIES_FLOWER_H_ */
