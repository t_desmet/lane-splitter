/*
 * bonusHealth.h
 *
 *  Created on: Jun 20, 2015
 *      Author: tom
 */

#ifndef MODEL_ENTITIES_BONUSHEALTH_H_
#define MODEL_ENTITIES_BONUSHEALTH_H_

#include "bonus.h"

namespace ls {
namespace model {
/**
 * \brief This is a specialization of Bonus and represents every Health bonus in the game.
 */
class BonusHealth: public Bonus
{
public:
	using Bonus::Bonus;

	void collide(Entity& other);
	std::string getName() const;
	std::string getSpritePath() const;
};

}
}

#endif /* SRC_MODEL_ENTITIES_BONUSHEALTH_H_ */
