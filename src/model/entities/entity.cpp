/*
 * entity.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "entity.h"
#include <cmath>

namespace ls {
namespace model {
/**
 * \brief The constructor for Entity.
 *
 * @param xPos The X coordinate of the entity.
 * @param yPos The Y coordinate of the entity.
 * @param velocity The vertical velocity of the entity.
 */
Entity::Entity(float xPos, float yPos, float velocity)
	: xPos(xPos), yPos(yPos), velocity(velocity), alive(true)
{
}

/**
 * The destructor for Entity.
 */
Entity::~Entity()
{
}

/**
 * \brief Detects whether this entity collides with the given one.
 *
 * @param other The other entity to detect collision with.
 * @return true when this entity collides with the given one.
 */
bool Entity::collidesWith(const Entity& other) const
{
	float overlapsX = std::abs(xPos - other.getXPos()) < (getWidth() + other.getWidth())/2;
	float overlapsY = std::abs(yPos - other.getYPos()) < (getHeight() + other.getHeight())/2;
	return (overlapsX && overlapsY);
}

/**
 * \brief Getter for the vertical velocity of this entity.
 *
 * @return The vertical velocity of this entity.
 */
float Entity::getVelocity() const
{
	return velocity;
}

/**
 * \brief Setter for the vertical velocity of this entity.
 *
 * @param velocity The vertical velocity of this entity.
 */
void Entity::setVelocity(float velocity)
{
	this->velocity = velocity;
}

/**
 * \brief Getter for the X coordinate of this entity.
 *
 * @return The X coordinate of this entity.
 */
float Entity::getXPos() const
{
	return xPos;
}

/**
 * \brief Setter for the X coordinate of this entity.
 *
 * @param x The X coordinate of this entity.
 */
void Entity::setXPos(float x)
{
	xPos = x;
}

/**
 * \brief Getter for the X coordinate of this entity.
 *
 * @return The X coordinate of this entity.
 */
float Entity::getYPos() const
{
	return yPos;
}

/**
 * \brief Setter for the Y coordinate of this entity.
 *
 * @param y The Y coordinate of this entity.
 */
void Entity::setYPos(float y)
{
	yPos = y;
}

/**
 * \brief Indicates whether this entity is alive.
 *
 * Dead entities should be deleted at some point to reuse memory.
 *
 * @return true if this entity is alive.
 */
bool Entity::isAlive() const
{
	return alive;
}

/**
 * \brief Kills this entity.
 *
 * This method resets this entity's alive state.
 */
void Entity::kill()
{
	alive = false;
}

}
}
