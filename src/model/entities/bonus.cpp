/*
 * bonus.cpp
 *
 *  Created on: Jun 20, 2015
 *      Author: Tom De Smet
 */

#include "bonus.h"

namespace ls {
namespace model {

std::string Bonus::getName() const
{
	return "Bonus";
}

void Bonus::update(float dt)
{
	yPos += dt * velocity;
}

float Bonus::getWidth() const
{
	return 0.50;
}

float Bonus::getHeight() const
{
	return 0.50;
}

}
}


