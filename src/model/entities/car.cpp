/*
 * car.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "car.h"

namespace ls {
namespace model {

/**
 * \brief The constructor for Car.
 *
 * @param xPos The X coordinate of the car.
 * @param yPos The Y coordinate of the car.
 * @param velocity The vertical velocity of the car.
 * @param health The health of the car.
 * @param ammo The ammo of the car.
 */
Car::Car(float xPos, float yPos, float velocity, float health, float ammo)
	: Entity(xPos, yPos, velocity), health(health), ammo(ammo)
{
}

/**
 * The destructor for Car.
 */
Car::~Car()
{
}

std::string Car::getName() const {
	return "Car";
}

void Car::update(float dt) {
	yPos += dt * velocity;
}

float Car::getWidth() const {
	return 0.75;
}

float Car::getHeight() const {
	return getWidth() * 2.042134831;
}

/**
 * \brief Getter for the ammo of cars.
 *
 * @return The ammo of the car.
 */
float Car::getAmmo() const {
	return ammo;
}

/**
 * \brief Setter for the ammo of cars.
 */
void Car::setAmmo(float ammo) {
	this->ammo = (ammo < 0) ? 0 : ammo;
}

/**
 * \brief Getter for the health of cars.
 *
 * @return The health of the car.
 */
float Car::getHealth() const {
	return health;
}

/**
 * \brief Setter for the health of cars.
 */
void Car::setHealth(float health) {
	this->health = (health < 0) ? 0 : health;
	if (this->health == 0)
		alive = false;
}
}
}
