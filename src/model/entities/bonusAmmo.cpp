/*
 * bonusAmmo.cpp
 *
 *  Created on: Jun 20, 2015
 *      Author: Tom De Smet
 */

#include "bonusAmmo.h"
#include "player.h"

namespace ls {
namespace model {

void BonusAmmo::collide(Entity& other)
{
	// Checks for collision with player.
	Player * player = dynamic_cast<Player *>(&other);
	if (player != NULL) {
		// Boosts the player's ammo.
		player->setAmmo(player->getAmmo()+5);
		// Remove this bonus.
		this->kill();
	}
}

std::string BonusAmmo::getName() const
{
	return "Ammo Bonus";
}

std::string BonusAmmo::getSpritePath() const
{
	return "resources/sprites/ammo.png";
}

}
}



