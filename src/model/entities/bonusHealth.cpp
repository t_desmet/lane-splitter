/*
 * bonusHealth.cpp
 *
 *  Created on: Jun 20, 2015
 *      Author: Tom De Smet
 */

#include "bonusHealth.h"
#include "player.h"

namespace ls {
namespace model {

void BonusHealth::collide(Entity& other)
{
	// Checks for collision with player.
	Player * player = dynamic_cast<Player *>(&other);
	if (player != NULL) {
		// Boosts the player's ammo.
		player->setHealth(player->getHealth()+50);
		// Remove this bonus.
		this->kill();
	}
}

std::string BonusHealth::getName() const
{
	return "Health Bonus";
}

std::string BonusHealth::getSpritePath() const
{
	return "resources/sprites/redCross.png";
}

}
}


