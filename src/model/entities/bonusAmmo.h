/*
 * bonusAmmo.h
 *
 *  Created on: Jun 20, 2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ENTITIES_BONUSAMMO_H_
#define MODEL_ENTITIES_BONUSAMMO_H_

#include "bonus.h"

namespace ls {
namespace model {
/**
 * \brief This is a specialization of Bonus and represents every Ammo bonus in the game.
 */
class BonusAmmo: public Bonus
{
public:
	using Bonus::Bonus;

	void collide(Entity& other);
	std::string getName() const;
	std::string getSpritePath() const;
};

}
}




#endif /* SRC_MODEL_ENTITIES_BONUSAMMO_H_ */
