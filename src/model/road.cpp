/*
 * Road.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "road.h"
#include "../game.h"
#include <cmath>
#include <iostream>

namespace ls {
namespace model {

/**
 * \brief The constructor for Road.
 *
 * @param width The width of this road.
 * @param height The height of this road.
 * @param width The width of this road.
 * @param playerHealth The health of the player.
 * @param playerAmmo The ammo of the player.
 * @param enemyHealth The health of enemy cars.
 * @param enemyAmmo The ammo of enemy cars.
 */
Road::Road(unsigned width, unsigned height, float playerHealth, float playerAmmo, float enemyHealth, float enemyAmmo)
	: width(width), height(height), playerHealth(playerHealth), playerAmmo(playerAmmo), enemyHealth(enemyHealth), enemyAmmo(enemyAmmo), randomShotTimer(0), tLastSpawn(0), spawnCounter(0), player(0, height - 1.5, playerHealth, playerAmmo, "Player")
{
}

/**
 * \brief Getter for the height of this road.
 *
 * @return The height of this road.
 */
unsigned Road::getHeight() const
{
	return height;
}

/**
 * \brief Getter for the width of this road.
 *
 * @return The width of this road.
 */
unsigned Road::getWidth() const
{
	return width;
}

/**
 * \brief Getter for the player on this road.
 *
 * @return The player on this road.
 */
Player& Road::getPlayer()
{
	return player;
}

/**
 * \brief Getter for the enemy cars on this road.
 *
 * @return The enemy cars on this road.
 */
std::list<EnemyCar>& Road::getEnemyCars()
{
	return enemyCars;
}

/**
 * \brief Getter for the weapon you fire.
 *
 * @return The shots that are fired.
 */
std::list<Flower>& Road::getFlowers()
{
	return flowers;
}

/**
 * \brief Getter for the health bonuses on the road.
 *
 * @return The health bonus.
 */
std::list<BonusHealth>& Road::getBonusHealths()
{
	return bonusHealths;
}

/**
 * \brief Getter for the ammo bonuses on the road.
 *
 * @return The ammo bonus.
 */
std::list<BonusAmmo>& Road::getBonusAmmos()
{
	return bonusAmmos;
}

/**
 * Getter for the path of the texture of this road.
 *
 * @return The path for the texture of this road.
 */
std::string Road::getTexturePath() const
{
	return "resources/textures/road.jpg";
}

/**
 * \brief Getter for the PlayerHealth.
 *
 * @return The player's health.
 */
float Road::getPlayerHealth() const
{
	return player.getHealth();
}

/**
 * \brief Setter for the PlayerHealth.
 *
 * @param health The amount of health to set.
 */
void Road::setPlayerHealth(float health)
{
	player.setHealth(health);
}

/**
 * \brief Updates the road and all of it entities for a given interval.
 *
 * @param dt The interval to update the model for.
 */
void Road::update(float dt)
{
	// Keep the timer since the last spawn.
	tLastSpawn += dt;

	// Keep the timer for random shots updated.
	randomShotTimer += dt;

	// Get a reference to the game singleton.
	Game& game = Game::getInstance();

	// Random enemy fire.
	if (randomShotTimer >= 1000) {
		for (auto& car : enemyCars) {
			if ((int) std::rand() % 2) {
				enemyFire(car);
			}
		}
		randomShotTimer = 0;
	}


	// Update the player for dt.
	player.update(dt);

	// Update all enemy cars for dt.
	for (auto& car : enemyCars) {
		car.update(dt);
	}

	// Update flowers fired for dt.
	for (auto& flower : flowers) {
		flower.update(dt);
	}

	// Update bonuses for dt.
	for (auto& bonusHealth : bonusHealths) {
		bonusHealth.update(dt);
	}
	for (auto& bonusAmmo : bonusAmmos) {
		bonusAmmo.update(dt);
	}

	// Handle all collisions.
	handleCollisions();

	// Kill all cars that drove of the road or dead.
	killDeadCars();

	// Kill shots that didn't hit and shots that hit.
	killDeadFlowers();

	// Kill missed bonuses and bonuses.
	killDeadBonuses();

	// Spawn a new enemy or bonus if necessary.
	if (tLastSpawn > 5000./(1 + std::log(game.getGameTime()/1000 + 1000)/10.)/(float)((int)game.getLevel()+1)) {
		tLastSpawn = 0;
		spawn();
	}

}

/**
 * \brief Spawn a new enemy on a given X coordinate.
 *
 * @param xPos The X coordinate to spawn the enemy at.
 */
void Road::spawn(float xPos)
{
	if (spawnCounter <= 3) {
		enemyCars.push_back(EnemyCar(xPos, 0, 0.003, enemyHealth, enemyAmmo));
		spawnCounter++;
	}

	else
	{
		if ((int) std::rand() % 2)
			bonusHealths.push_back(BonusHealth(xPos, 0, 0.003));
		else
			bonusAmmos.push_back(BonusAmmo(xPos, 0, 0.003));
		spawnCounter = 0;
	}
}

/**
 * \brief Spawn a new enemy on a random X coordinate.
 */
void Road::spawn()
{
	float xPos = getWidth() * (float) std::rand() / (float) RAND_MAX - getWidth() / 2;
	spawn(xPos);
}

/**
 * \brief Spawn a new flower on a given X and Y coordinate.
 *
 * @param xPos The X coordinate to spawn the flower at.
 * @param yPos The Y coordinate to spawn the flower at.
 * @param velocity Velocity of the flower.
 */
void Road::spawnFlower(float xPos, float yPos, float velocity)
{
	flowers.push_back(Flower(xPos, yPos, velocity));
}

/**
 * \brief Spawn a new flower as a shot from the player car.
 */
void Road::playerFire()
{
	if(player.getAmmo() > 0) {
		float yPos = getHeight() - player.getHeight() -1;
		float xPos = player.getXPos();
		spawnFlower(xPos, yPos, 0.006);
		player.setAmmo(player.getAmmo() - 1);
	}
}

/**
 * \brief Spawn a new flower as a shot from the enemy car.
 *
 * @param car The enemy car.
 */
void Road::enemyFire(EnemyCar& car)
{
	if(car.getAmmo() > 0) {
		float yPos = car.getHeight()+ car.getYPos();
		float xPos = car.getXPos();
		spawnFlower(xPos, yPos, -0.006);
		car.setAmmo(car.getAmmo() - 1);
	}
}

/**
 * \brief Kill all cars that drove off the road.
 */
void Road::killDeadCars()
{
	for (std::list<EnemyCar>::iterator it = enemyCars.begin(); it != enemyCars.end(); it++) {
		if ((it->getYPos() > height) || !it->isAlive()) {
			it = enemyCars.erase(it);
			it--;
		}
	}
}

/**
 * \brief Kill all shots that missed.
 */
void Road::killDeadFlowers()
{
	for (std::list<Flower>::iterator it = flowers.begin(); it != flowers.end(); it++) {
		if ((it->getYPos() < 0) || !it->isAlive()) {
			it = flowers.erase(it);
			it--;
		}
	}
}

/**
 * \brief Kill bonuses missed.
 */
void Road::killDeadBonuses()
{
	for (std::list<BonusHealth>::iterator it = bonusHealths.begin(); it != bonusHealths.end(); it++) {
		if ((it->getYPos() > height) || !it->isAlive()) {
			it = bonusHealths.erase(it);
			it--;
		}
	}
	for (std::list<BonusAmmo>::iterator it = bonusAmmos.begin(); it != bonusAmmos.end(); it++) {
		if ((it->getYPos() > height) || !it->isAlive()) {
			it = bonusAmmos.erase(it);
			it--;
		}
	}
}

/**
 * \brief Trigger all collisions between the player, enemy cars, bonuses and weapons.
 */
void Road::handleCollisions()
{
	// Loop through the enemyCars.
	for (auto& car : enemyCars)
	{
		// Check for collision with the player.
		if (player.collidesWith(car)) {
			car.collide(player);
		}
		// Loop through the flowers.
		for (auto& flower : flowers)
		{
			// check for collisions with enemy cars.
			if (car.collidesWith(flower)) {
				flower.collide(car);
			}
		}
	}

	// Loop through the flowers.
	for (auto& flower : flowers)
	{
		// Check for collision with the player.
		if (player.collidesWith(flower)) {
			flower.collide(player);
		}
	}

	// Loop through health bonuses.
	for (auto& healthBonus : bonusHealths)
	{
		// Check for collision with the player.
		if (player.collidesWith(healthBonus)) {
				healthBonus.collide(player);
			}
	}

	// Loop through ammo bonuses.
	for (auto& ammoBonus : bonusAmmos)
	{
		// Check for collision with the player.
		if (player.collidesWith(ammoBonus)) {
				ammoBonus.collide(player);
			}
	}

}
}
}
