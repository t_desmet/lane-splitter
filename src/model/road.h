/*
 * road.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef MODEL_ROAD_H_
#define MODEL_ROAD_H_

#include "../model/entities/player.h"
#include <list>
#include "entities/enemyCar.h"
#include "entities/flower.h"
#include "entities/bonusHealth.h"
#include "entities/bonusAmmo.h"

namespace ls {
/**
 * \brief The namespace where all code for the \ref Road "Model" component resides.
 * This is the 'M' in 'MVC'.
 */
namespace model {
/**
 * \brief The Road class.
 *
 * For now this class contains the entire model for the game.
 * The members of this class are now the player and enemy cars.
 */
class Road
{
public:
	Road(unsigned width = 4, unsigned height = 12, float playerHealth = 100, float playerAmmo = 10, float enemyHealth = 20, float enemyAmmo = 1);
	unsigned getHeight() const;
	unsigned getWidth() const;
	Player& getPlayer();
	std::list<EnemyCar>& getEnemyCars();
	std::list<Flower>& getFlowers();
	std::list<BonusHealth>& getBonusHealths();
	std::list<BonusAmmo>& getBonusAmmos();
	std::string getTexturePath() const;
	float getPlayerHealth() const;
	void setPlayerHealth(float health);
	void update(float dt);
	void spawn(float xPos);
	void spawn();
	void spawnFlower(float xPos, float yPos, float velocity);
	void playerFire();
	void enemyFire(EnemyCar& car);
	void killDeadCars();
	void killDeadFlowers();
	void killDeadBonuses();
	void handleCollisions();
private:
	/**
	 * The width of the road.
	 */
	const unsigned width;
	/**
	 * The height of the road.
	 */
	const unsigned height;
	/**
	 * The health of the player.
	 */
	float playerHealth;
	/**
	 * The ammo of the player.
	 */
	float playerAmmo;
	/**
	 * The health of the enemy.
	 */
	float enemyHealth;
	/**
	 * The ammo of the enemy.
	 */
	float enemyAmmo;
	/**
	 * Time since last spawn.
	 */
	float tLastSpawn;
	/**
	 * Timer for random enemy shots.
	 */
	float randomShotTimer;
	/**
	 * Amount of spawns.
	 */
	float spawnCounter;
	/**
	 * The player.
	 */
	Player player;
	/**
	 * List of all enemy cars on the field.
	 */
	std::list<EnemyCar> enemyCars;
	/**
	 * List of all flowers on the field.
	 */
	std::list<Flower> flowers;
	/**
	 * List of all health bonuses on the field.
	 */
	std::list<BonusHealth> bonusHealths;
	/**
	 * List of all ammo bonuses on the field.
	 */
	std::list<BonusAmmo> bonusAmmos;
};
}
}

#endif /* MODEL_ROAD_H_ */
