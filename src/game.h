/*
 * game.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef GAME_H_
#define GAME_H_

#include "model/road.h"
#include "view/sdlWindow.h"
#include "controller/sdlController.h"

/**
 * \brief The namespace for all code related to the Lane Splitter game.
 */
namespace ls {

/** This enum represents the different game levels. */
enum class GameLevel {
	kRookie = 0,
	kBeginner = 1,
	kIntermediate = 2,
	kPro = 3,
	kImpossible = 4,

	Count
};

/**
 * \brief This singleton contains the \ref model::Road "model", \ref view::View "view" and \ref controller::Controller "controller" for the game.
 *
 * The Game class can only be constructed as a singleton. It contains all logic
 * that does not belong to the MVC, and thus is not inherent to the game itself,
 * like when the game ends, statistics, ...
 */
class Game
{
public:
	static Game& getInstance();
	void run();
	void stop();
	void pause();
	float getPlayerHealth() const;
	void setPlayerHealth(float playerHealth);
	GameLevel getLevel() const;
	void setLevel(GameLevel level);
	float getGameTime() const;
private:
	Game();
	virtual ~Game();

	Game(Game const&) = delete;
	void operator=(Game const&) = delete;

	void update(float dt);

	void updateModel(float dt);
	void updateView(float dt);
	void updateController();

	/** The running state of the game. */
	bool bRunning;
	/** The pause state of the game */
	bool bPause;
	/** The number of frames to compute per second */
	float fps;

	/** A pointer to the model. */
	model::Road * road;
	/** A pointer to the view. */
	view::Window * view;
	/** A pointer to the controller. */
	controller::Controller * controller;

	/** Time since start of the game. */
	float gameTime;

	/** The level of this game. */
	GameLevel level;
};
}

#endif /* GAME_H_ */
