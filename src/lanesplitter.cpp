//============================================================================
// Name        : LaneSplitter.cpp
// Author      : Tom De Smet
// Version     :
// Description : The entry point for the LaneSplitter game.
//============================================================================

#include "game.h"
#include <iostream>

int main(int argc, char *argv[])
{
	// Get a reference to the Game singleton.
	ls::Game& game = ls::Game::getInstance();
	// Set the difficulty for this game.
	int level;
	do {
		std::cout << "Enter desired game level(1-5): ";
		std::cin >> level;
	} while (!level > 0 && !level <= 5);
		switch (level) {
		case 1:
				game.setLevel(ls::GameLevel::kRookie);
				game.setPlayerHealth(300);
				break;
		case 2:
				game.setLevel(ls::GameLevel::kBeginner);
				game.setPlayerHealth(250);
				break;
		case 3:
				game.setLevel(ls::GameLevel::kIntermediate);
				game.setPlayerHealth(200);
				break;
		case 4:
				game.setLevel(ls::GameLevel::kPro);
				game.setPlayerHealth(150);
				break;
		case 5:
				game.setLevel(ls::GameLevel::kImpossible);
				game.setPlayerHealth(100);
				break;
	}
	// Start running the game loop.
	game.run();

	std::cout << "Game finished after " << game.getGameTime()/1000. << " seconds." << std::endl;

	return EXIT_SUCCESS;
}
