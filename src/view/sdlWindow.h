/*
 * sdlWindow.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef VIEW_SDL_SDLWINDOW_H_
#define VIEW_SDL_SDLWINDOW_H_

#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "window.h"

namespace ls {
namespace view {
/**
 * \brief This class specializes the Window class for SDL 2.
 */
class SDLWindow: public Window
{
public:
	/**
	 * Reuse the constructor from Window.
	 */
	using Window::Window;

	virtual ~SDLWindow();
	bool create();
	void close();
	void clear();
	void render();
	void drawSprite(unsigned xPos, unsigned yPos, unsigned width, unsigned height, int rotation, std::string spritePath);
	void drawVerticallyTiledBackground(unsigned width, unsigned int height, std::string texturePath);
	void drawText(unsigned xPos, unsigned yPos, float scale, std::string text);
private:
	/** A pointer to the SDL_Window instance. */
	SDL_Window* pWindow;
	/** A pointer to the SDL_Renderer instance. */
	SDL_Renderer *pRenderer;
	/** A cache for the already loaded textures. */
	std::map<std::string, SDL_Texture*> textureMap;

	TTF_Font* timeFont;
};
}
}

#endif /* SDLVIEW_H_ */
