/*
 * sdlWindow.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include <cmath>
#include <SDL2/SDL_image.h>
#include "sdlWindow.h"

namespace ls {
namespace view {

/**
 * \brief The destructor for SDLWindow.
 */
SDLWindow::~SDLWindow() {
	// Use the close method to clean everything up
	close();
}

bool SDLWindow::create() {
	TTF_Init();
	timeFont = TTF_OpenFont("resources/fonts/FreeSans.ttf", 24); //this opens a font style and sets a size

	// Try to create an SDL window. Clean up and return false on failure.
	pWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
	if (!pWindow) {
		SDL_Quit();
		return false;
	}

	// Try to create an SDL renderer. Clean up and return false on failure.
	pRenderer = SDL_CreateRenderer(pWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!pRenderer) {
		SDL_DestroyWindow(pWindow);
		SDL_Quit();
		return false;
	}

	// Set the default background color to black.
	SDL_SetRenderDrawColor(pRenderer, 0, 0, 0, 255);

	// Everything succeeded if we got here, so return true.
	return true;
}

void SDLWindow::close() {
	// Free all the textures in the cache.
	for (auto& entry : textureMap)
		SDL_DestroyTexture(entry.second);

	// If there is a renderer, destroy it and reset the pointer to it.
	if (pRenderer != 0) {
		SDL_DestroyRenderer(pRenderer);
		pRenderer = 0;
	}

	// If there is a window, destroy it and reset the pointer to it.
	if (pWindow != 0) {
		// Destroy window
		SDL_DestroyWindow(pWindow);
		pWindow = 0;
	}

	// Quit SDL subsystems
	SDL_Quit();
}

void SDLWindow::clear() {
	//Clear the screen
	SDL_RenderClear(pRenderer);
}

void SDLWindow::render() {
	//Update the screen
	SDL_RenderPresent(pRenderer);
}

void SDLWindow::drawSprite(unsigned xPos, unsigned yPos, unsigned w, unsigned h,
		int rotation, std::string spritePath) {
	// Prepare a pointer for the texture.
	SDL_Texture* texture = 0;

	// Try loading the texture from cache, otherwise load it from the file and
	// store it in the cache.
	if (textureMap.find(spritePath) != textureMap.end()) {
		texture = textureMap.at(spritePath);
	} else {
		SDL_Surface * surf = IMG_Load(spritePath.c_str());
		texture = SDL_CreateTextureFromSurface(pRenderer, surf);
		SDL_FreeSurface(surf);
		textureMap[spritePath] = texture;
	}

	// Define the rectangle to draw the texture in.
	SDL_Rect roi;
	roi.x = width / 2 + xPos - w / 2;
	roi.y = yPos - h / 2;
	roi.w = w;
	roi.h = h;

	//Draw the texture
	SDL_RenderCopyEx(pRenderer, texture, NULL, &roi, rotation, NULL,
			SDL_FLIP_NONE);

}

void SDLWindow::drawVerticallyTiledBackground(unsigned w, unsigned int h,
		std::string texturePath) {
	// Prepare a pointer for the texture.
	SDL_Texture* texture = 0;

	// Try loading the texture from cache, otherwise load it from the file and
	// store it in the cache.
	if (textureMap.find(texturePath) != textureMap.end()) {
		texture = textureMap.at(texturePath);
	} else {
		SDL_Surface *surf = IMG_Load(texturePath.c_str());
		texture = SDL_CreateTextureFromSurface(pRenderer, surf);
		SDL_FreeSurface(surf);
		textureMap[texturePath] = texture;
	}

	// Query for the width and height of the loaded texture.
	int texW, texH;
	SDL_QueryTexture(texture, NULL, NULL, &texW, &texH);

	// Determine the ratio of the texture.
	double texRatio = texH / texW;

	// Calculate the number of vertical repetitions to fill the screen.
	unsigned nTile = std::ceil( (float) height / (float) w / texRatio );

	// Draw each tile of the texture.
	for (unsigned i = 0; i < nTile + 1; i++) {
		// Define the rectangle to draw the current tile in.
		SDL_Rect roi;
		roi.x = width / 2 - w / 2;
		roi.y = i * w * texRatio;
		roi.w = w;
		roi.h = w * texRatio;

		//Draw the texture
		SDL_RenderCopyEx(pRenderer, texture, NULL, &roi, 0, NULL,
				SDL_FLIP_NONE);
	}
}

void SDLWindow::drawText(unsigned xPos, unsigned yPos, float scale, std::string text) {

	if (!timeFont)
		throw std::runtime_error("TrueType Font not found!");

	SDL_Color White = {255, 255, 255};  // this is the color in rgb format, maxing out all would give you the color white, and it will be your text's color

	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(timeFont, text.c_str(), White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

	SDL_Texture* Message = SDL_CreateTextureFromSurface(pRenderer, surfaceMessage); //now you can convert it into a texture

	SDL_Rect Message_rect; //create a rect
	Message_rect.x = xPos;  //controls the rect's x coordinate
	Message_rect.y = yPos; // controls the rect's y coordinte
	Message_rect.w = 15*text.length(); // controls the width of the rect
	Message_rect.h = 20; // controls the height of the rect

	//Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understance

	//Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

	SDL_RenderCopy(pRenderer, Message, NULL, &Message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
}
}
}
