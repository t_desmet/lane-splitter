/*
 * view.h
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#ifndef VIEW_WINDOW_H_
#define VIEW_WINDOW_H_

#include "../model/road.h"
#include <string>

namespace ls {
namespace view {
/**
 * \brief This abstract class contains helper methods to create a graphic
 * window and render entities and textures on it.
 *
 * It has some virtual methods that needs to be specialized for multimedia
 * libraries like SDL, SFML, DirectX, ...
 */
class Window {
public:
	Window(unsigned width, unsigned height, std::string title = "");

	void drawRoad(model::Road& road);
	void drawTime(float time);
	void drawPlayerHealth(float Health);
	void drawPlayerAmmo(float Ammo);

	/**
	 * \brief The destructor for Window.
	 */
	virtual ~Window() = default;

	/**
	 * \brief Create the window on screen.
	 *
	 * @return true if creation succeeded.
	 */
	virtual bool create() = 0;

	/**
	 * \brief Close the window.
	 */
	virtual void close() = 0;

	/**
	 * \brief Clears all textures from the window.
	 */
	virtual void clear() = 0;

	/**
	 * \brief Render all drawn textures since the last clear.
	 */
	virtual void render() = 0;
protected:
	void drawEntity(model::Entity& entity, float scale);
	void drawRoadTexture(model::Road& road, float scale);

	/**
	 * \brief Draw the texture for an entity.
	 *
	 * @param xPos The X coordinate on screen.
	 * @param yPos The Y coordinate on screen.
	 * @param width The width on screen.
	 * @param height The height on screen.
	 * @param rotation The rotation in degrees.
	 * @param spritePath The path of the texture to draw.
	 */
	virtual void drawSprite(unsigned xPos, unsigned yPos, unsigned width,
			unsigned height, int rotation, std::string spritePath) = 0;

	/**
	 * \brief Draw a texture vertically tiled.
	 *
	 * @param width The width of the texture on screen.
	 * @param height The height of the texture on screen.
	 * @param texturePath The path of the texture to draw.
	 */
	virtual void drawVerticallyTiledBackground(unsigned width,
			unsigned int height, std::string texturePath) = 0;

	/**
	 * \brief Draw the texture for an entity.
	 *
	 * @param xPos The X coordinate on screen.
	 * @param yPos The Y coordinate on screen.
	 * @param scale The size of the text.
	 * @param text The text.
	 */
	virtual void drawText(unsigned xPos, unsigned yPos, float scale,
			std::string text) = 0;

private:
	double getProjectionScale(unsigned gridWidth, unsigned gridHeight) const;

protected:
	/** The width of the window. */
	unsigned width;
	/** The height of the window. */
	unsigned height;
	/** The title to display on the window. */
	std::string title;
};
}
}
#endif /* WINDOW_H_ */
