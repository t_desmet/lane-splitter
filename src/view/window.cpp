/*
 * view.cpp
 *
 *  Created on: 30-apr.-2015
 *      Author: Tom De Smet
 */

#include "window.h"

namespace ls {
namespace view {
/**
 * \brief The constructor for Window.
 *
 * @param width The width of the window.
 * @param height The height of the window.
 * @param title The title to display on the window.
 */
Window::Window(unsigned width, unsigned height, std::string title) :
		width(width), height(height), title(title) {
}

/**
 * \brief Returns the scale factor from the road coordinates to screen coordinates.
 *
 * @param gridWidth The width of the road. (unused)
 * @param gridHeight The height of the road.
 */
double Window::getProjectionScale(unsigned gridWidth,
		unsigned gridHeight) const {
	return height / gridHeight;
}

/**
 * \brief Draw an entity from the model.
 *
 * @param entity The entity to draw.
 * @param scale The scale to resize the texture with.
 */
void Window::drawEntity(model::Entity& entity, float scale) {
	// Call the specialized method to draw the sprite.
	drawSprite(entity.getXPos() * scale, entity.getYPos() * scale,
			entity.getWidth() * scale, entity.getHeight() * scale, 0,
			entity.getSpritePath());
}

/**
 * \brief Draw the entire road model.
 *
 * This draws the road texture and all of it entities.
 *
 * @param road The road to draw.
 */
void Window::drawRoad(model::Road& road) {
	// Determine the projection scale.
	float scale = getProjectionScale(road.getWidth(), road.getHeight());

	// Draw the texture for the road.
	drawRoadTexture(road, scale);

	// Draw the player.
	drawEntity(road.getPlayer(), scale);

	// Draw all enemies.
	for (auto& car : road.getEnemyCars())
		drawEntity(car, scale);

	// Draw flowers.
	for (auto& weapon : road.getFlowers())
		drawEntity(weapon, scale);

	// Draw bonuses.
	for (auto& healthBonus : road.getBonusHealths())
		drawEntity(healthBonus, scale);
	for (auto& ammoBonus : road.getBonusAmmos())
		drawEntity(ammoBonus, scale);

}

/**
 * \brief Draw the texture for the road.
 *
 * @param road The road to draw the texture for.
 * @param scale The scale to resize the texture with.
 */
void Window::drawRoadTexture(model::Road& road, float scale) {
	// Call the specialized method to draw the tiled background.
	drawVerticallyTiledBackground(width * road.getWidth() / road.getHeight(),
			height, road.getTexturePath());
}
/**
 * \brief Draw the points.
 *
 * @param time The time to show as points.
 */
void Window::drawTime(float time)
{
	std::string text = "Points: " + std::to_string((int)time);
	drawText(width-200, 5, 1, text);
}
/**
 * \brief Draw the player's health.
 *
 * @param Health The health of the player.
 */
void Window::drawPlayerHealth(float Health)
{
	std::string text = "Health: " + std::to_string((int)Health);
	drawText(width-200, 35, 1, text);
}
/**
 * \brief Draw the player's ammo.
 *
 * @param Ammo The ammo of the player.
 */
void Window::drawPlayerAmmo(float Ammo)
{
	std::string text = "Ammo: " + std::to_string((int)Ammo);
	drawText(width-200, 65, 1, text);
}

}
}
