/*
 * view.cpp
 *
 *  Created on: 9-mei-2015
 *      Author: Tom De Smet
 */

#include "view.h"

namespace ls {
namespace view {
/**
 * \brief The constructor for View.
 *
 * @param width The width of the window.
 * @param height The height of the window.
 * @param title The title to display on the window.
 */
View::View(unsigned width, unsigned height, std::string title)
	: window(new SDLWindow(width, height, title))
{
}

/**
 * \brief The destructor for View.
 */
View::~View()
{
	delete window;
}

/**
 * \brief Getter for the Window pointer.
 *
 * @return A pointer to the Window instance.
 */
Window * View::getWindow()
{
	return window;
}
}
}
