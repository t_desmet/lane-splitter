/*
 * view.h
 *
 *  Created on: 9-mei-2015
 *      Author: Tom De Smet
 */

#ifndef VIEW_VIEW_H_
#define VIEW_VIEW_H_

#include "window.h"
#include "sdlWindow.h"

namespace ls
{
/**
 * \brief The namespace where all code for the View component resides.
 * This is the 'V' in 'MVC'.
 */
namespace view {
/**
 * \brief The View class.
 * Right now it holds a single window that renders the road and cars.
 */
class View
{
public:
	View(unsigned width, unsigned height, std::string title);
	virtual ~View();
	Window * getWindow();
private:
	/**
	 * A pointer to the game Window.
	 */
	Window * window;
};
}
}

#endif /* VIEW_VIEW_H_ */
